import { BaseSyntheticEvent, ChangeEventHandler, useState } from "react";
import UserRegistrationFormInterFace from "../types/UserRegistrationFormInterface";

function useUserRegistrationFormHook(initialState: any) {
  const [values, setValues] = useState(initialState);

  function handleChange(e: BaseSyntheticEvent) {
    setValues((prevValues: UserRegistrationFormInterFace) => {
      const shouldSendEmail =
        e.target.name === "shouldSendEmail"
          ? !prevValues.shouldSendEmail
          : prevValues.shouldSendEmail;

      const newState = {
        ...prevValues,
        [e.target.name]: e.target.value,
        shouldSendEmail,
      };

      return newState;
    });
  }

  return [values, handleChange];
}

export default useUserRegistrationFormHook;
