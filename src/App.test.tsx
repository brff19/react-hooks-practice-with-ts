import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders User Registration Form", () => {
  render(<App />);

  const legendText = screen.queryByTestId("legend")?.textContent;

  expect(legendText).toContain("User Registration Form");
});
