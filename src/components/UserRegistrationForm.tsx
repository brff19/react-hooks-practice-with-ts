import useUserRegistrationFormHook from "../hooks/useUserRegistrationFormHook";

function UserRegistrationForm() {
  const [
    { email, password, hasAgreedToTerms, shouldSendEmail },
    handleFormChange,
  ] = useUserRegistrationFormHook({
    email: "",
    password: "",
    hasAgreedToTerms: false,
    shouldSendEmail: false,
  });

  return (
    <form onSubmit={(e) => e.preventDefault()}>
      <fieldset>
        <legend data-testid="legend">User Registration Form</legend>

        <label htmlFor="email">Email </label>
        <input
          type="text"
          id="email"
          onChange={handleFormChange}
          name="email"
          value={email}
        />
        <br />
        <label htmlFor="password">Password </label>
        <input
          type="text"
          id="password"
          onChange={handleFormChange}
          name="password"
          value={password}
        />
        <br />
        <label htmlFor="hasAgreedToTerms">Agree to Terms</label>
        <input
          type="radio"
          value="true"
          name="hasAgreedToTerms"
          id="hasAgreedToTerms"
          onChange={handleFormChange}
        />
        <br />
        <label htmlFor="shouldSendEmail">Agree to Terms</label>
        <input
          type="checkbox"
          name="shouldSendEmail"
          id="shouldSendEmail"
          checked={shouldSendEmail}
          onChange={handleFormChange}
        />
        <br />
        <button onClick={(e) => e.preventDefault()}>Register User</button>
        <pre>
          email: <kbd>{email}</kbd>
          password: <kbd>{password}</kbd>
        </pre>
      </fieldset>
    </form>
  );
}

export default UserRegistrationForm;
