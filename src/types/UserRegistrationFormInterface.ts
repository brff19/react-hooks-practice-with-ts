export default interface UserRegistrationFormInterFace {
  email: string;
  password: string;
  hasAgreedToTerms: boolean;
  shouldSendEmail: boolean;
}
